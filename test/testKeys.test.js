const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const getFun = require('../keys');

const testData = Object.keys(testObject);
const outputData = getFun.keys(testObject);

if (JSON.stringify(testData) === JSON.stringify(outputData)) {
    console.log('Test Passed!');
} else {
    console.log('Test Failed!');
}