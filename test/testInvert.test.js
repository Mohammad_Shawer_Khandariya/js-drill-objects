const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const _ = require('underscore');
const getFun = require('../invert');

const testData = _.invert(testObject);
const outputData = getFun.inverts(testObject);


if (JSON.stringify(testData) === JSON.stringify(outputData)) {
    console.log('Test Passed!');
} else {
    console.log('Test Failed!');
}