const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const _ = require('underscore');
const getFun = require('../pairs');

const testData = _.pairs(testObject);
const outputData = getFun.pairs(testObject);

if (JSON.stringify(testData) === JSON.stringify(outputData)) {
    console.log('Test Passed!');
} else {
    console.log('Test Failed!');
}