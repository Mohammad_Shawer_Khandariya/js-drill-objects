const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const _ = require('underscore');
const getFun = require('../mapObject');

const testData = _.mapObject(testObject, strToLen);
const outputData = getFun.mapObject(testObject, strToLen);

function strToLen(data) {
    if (typeof(data) === "string") {
        return(data.length);
    }
    return(data);
}

if (JSON.stringify(testData) === JSON.stringify(outputData)) {
    console.log('Test Passed!');
} else {
    console.log('Test Failed!');
}