const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const getFun = require('../values');

const testData = Object.values(testObject);
const outputData = getFun.values(testObject);

if (JSON.stringify(testData) === JSON.stringify(outputData)) {
    console.log('Test Passed!');
} else {
    console.log('Test Failed!');
}