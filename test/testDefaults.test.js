const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const sampleObj = { name: 'Shawer', Contact: '+91 7898989898', age: 22, enemy: "Joker"};
const _ = require('underscore');
const getFun = require('../defaults');

const outputData = getFun.defaults(testObject, sampleObj);

const testData = _.defaults(testObject, sampleObj);

if (JSON.stringify(testData) === JSON.stringify(outputData)) {
    console.log('Test Passed!');
} else {
    console.log('Test Failed!');
}