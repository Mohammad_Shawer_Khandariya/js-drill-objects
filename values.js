function values(obj) {
    let valueArray = [];
    for (let key in obj) {
        valueArray.push(obj[key]);
    }
    return(valueArray);
}

module.exports = {
    values
}