function inverts(obj) {
    let reverse = {}
    for (let item in obj) {
        reverse[obj[item]] = item;
    }
    return(reverse);
}

module.exports = {
    inverts
}