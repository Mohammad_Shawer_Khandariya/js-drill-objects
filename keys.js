function keys(obj) {
    let keyArray = []
    for (let property in obj) {
        keyArray.push(property);
    }
    return(keyArray);
}

module.exports = {
    keys
}