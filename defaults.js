function defaults(obj, defaultProps) {
    let newObj = JSON.parse(JSON.stringify(obj)); //deep copy
    for (let key in defaultProps) {
        if(obj[key] === undefined) {
            newObj[key] = defaultProps[key];
        }
    }
    return(newObj);
}

module.exports = {
    defaults
}