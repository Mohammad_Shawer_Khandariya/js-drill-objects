function pairs(obj) {
    let arrObj = [];
    for (let key in obj) {
        arrObj.push([key, obj[key]]);
    }
    return(arrObj);
}

module.exports = {
    pairs
}